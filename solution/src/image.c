//#include <inttypes.h>
//#include <malloc.h>
//#include <malloc.h>
#include "../include/image.h"

struct image rotate(struct image image, int angle) {
    struct image rotated_image;
    if (angle % 180 == 0) {
        rotated_image = (struct image) {
                image.width,
                image.height,
                malloc((sizeof(struct pixel)) * image.height * image.width)
        };
        if (rotated_image.data == NULL) {
            return (struct image) {0};
        }
    } else {
        rotated_image = (struct image) {
                image.height,
                image.width,
                malloc((sizeof(struct pixel)) * image.height * image.width)
        };
    }

    if (angle == 90) {
        for (uint32_t i = 0; i < image.height; i++) {
            for (uint32_t j = 0; j < image.width; j++) {
                uint32_t x = rotated_image.height - j - 1;
                uint32_t y = i;
                rotated_image.data[x * rotated_image.width + y].b = image.data[i * image.width + j].b;
                rotated_image.data[x * rotated_image.width + y].g = image.data[i * image.width + j].g;
                rotated_image.data[x * rotated_image.width + y].r = image.data[i * image.width + j].r;
            }
        }
    } else if (angle == 180) {
        for (uint32_t i = 0; i < image.height; i++) {
            for (uint32_t j = 0; j < image.width; j++) {
                uint32_t x = rotated_image.height - i - 1;
                uint32_t y = rotated_image.width - j - 1;
                rotated_image.data[x * rotated_image.width + y].b = image.data[i * image.width + j].b;
                rotated_image.data[x * rotated_image.width + y].g = image.data[i * image.width + j].g;
                rotated_image.data[x * rotated_image.width + y].r = image.data[i * image.width + j].r;
            }
        }
    } else if (angle == 270) {
        for (uint32_t i = 0; i < image.height; i++) {
            for (uint32_t j = 0; j < image.width; j++) {
                uint32_t x = j;
                uint32_t y = rotated_image.width - i - 1;
                rotated_image.data[x * rotated_image.width + y].b = image.data[i * image.width + j].b;
                rotated_image.data[x * rotated_image.width + y].g = image.data[i * image.width + j].g;
                rotated_image.data[x * rotated_image.width + y].r = image.data[i * image.width + j].r;
            }
        }
    } else {
        for (uint32_t i = 0; i < image.height; i++) {
            for (uint32_t j = 0; j < image.width; j++) {
                rotated_image.data[i * rotated_image.width + j].b = image.data[i * image.width + j].b;
                rotated_image.data[i * rotated_image.width + j].g = image.data[i * image.width + j].g;
                rotated_image.data[i * rotated_image.width + j].r = image.data[i * image.width + j].r;
            }
        }
    }

    return rotated_image;
}

