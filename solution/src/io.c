#include <stdlib.h>
//#include "io.h"


long get_angle(char *arg) {
    long angle = strtol(arg, NULL, 10);
    if (angle > -270 && angle < 0) {
        angle += 360;
    }

    return angle;
}
