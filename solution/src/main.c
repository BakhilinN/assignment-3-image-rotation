#include "io.h"
#include "bmp.h"


int main(int argc, char *argv[]) {

    if (argc == 4) {
        // initialization main variables
        struct image image = {0}; // STRUCT
        struct bmp_header bmpHeader = {0}; // STRUCT

        //get ANGLE
        int angle = get_angle(argv[3]);

        FILE *file = fopen(argv[1], "rb");


//        from_bmp(file, &bmpHeader, &image);
        if (from_bmp(file, &bmpHeader, &image) != 0) {
            printf("Your file not found. Check that you passed to the program correct filepath/");
            fclose(file);
            exit(1);
        }

        fclose(file);

        FILE *file1 = fopen(argv[2], "wb");
        struct image rotated = rotate(image, angle);
        free(image.data);

        if (to_bmp(file1, &bmpHeader, &rotated) != 0) {
            fclose(file1);
            exit(1);
        }
        free(rotated.data);
        fclose(file1);
    } else {
        printf("Wrong count of arguments! %d", argc);
        exit(WRONG_COUNT_OF_ARGUMENTS);
    }

    printf("https://httpcats.com/499.jpg");

    return 0;
}
