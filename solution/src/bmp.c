#define COUNT_OF_BYTES 22

#include <malloc.h>
//#include "../include/image.h"
#include "bmp.h"



enum read_status from_bmp(FILE *in, struct bmp_header *header, struct image *image) {

    size_t flag_fread = fread(header, sizeof(struct bmp_header), 1, in);

    if (flag_fread != 1) {
        free(image->data);
        fclose(in);
        return READ_INVALID_PIXEL;
    }

    *image = (struct image) {
            header->biWidth,
            header->biHeight,
            malloc((sizeof(struct pixel)) * header->biHeight * header->biWidth)
    };

    int flag_fseek = fseek(in, header->bOffBits, SEEK_SET);

    if (flag_fseek != 0) {
        free(image->data);
        fclose(in);
        return READ_INVALID_PIXEL;
    }


    for (uint32_t i = 0; i < header->biHeight; i = i + 1) {
        flag_fread = fread(&image->data[i * header->biWidth], sizeof(struct pixel) * header->biWidth, 1, in);
        if (flag_fread != 1) {
            free(image->data);
            fclose(in);
            return READ_INVALID_PIXEL;
        }

        flag_fseek = fseek(in, header->biWidth % 4, SEEK_CUR);

        if (flag_fseek != 0) {
            free(image->data);
            fclose(in);
            return READ_INVALID_PIXEL;
        }

        uint8_t p = header->biWidth % 4;
        fwrite(&p, sizeof(p), COUNT_OF_BYTES, in);
    }
    return READ_OK;
}

enum write_status to_bmp(FILE *out, struct bmp_header *header, struct image *image) {
    header->biHeight = image->height;
    header->biWidth = image->width;

    uint8_t padding = image->width % 4;
    uint8_t *padding_array[4] = {0};

    fwrite(header, sizeof(struct bmp_header), 1, out);

    for (uint32_t i = 0; i < image->height; i = i + 1) {
        size_t flag_fwrite = fwrite(&image->data[i * image->width], sizeof(struct pixel) * image->width, 1, out);
        if (flag_fwrite != 1) {
            free(image->data);
            fclose(out);
            return WRITE_ERROR;
        }

        fwrite(padding_array, sizeof(padding), padding, out);
    }

    fwrite(padding_array, sizeof(padding), padding - 1, out);
    return WRITE_OK;
}
