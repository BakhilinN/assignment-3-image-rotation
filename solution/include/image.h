#ifndef IMAGE_H
#define IMAGE_H


#include <stdint.h>
#include <stdlib.h>

#pragma pack(push, 1)
struct pixel {
    uint8_t b, g, r;
};
#pragma pack(pop)

struct image {
    uint64_t width, height;
    struct pixel *data;
};

struct image rotate(struct image image, int angle);

#endif
