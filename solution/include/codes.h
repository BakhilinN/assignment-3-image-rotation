

#ifndef CODES_H
#define CODES_H

#include <stdio.h>

/*  deserializer   */
enum read_status {
    READ_OK = 0,
    READ_INVALID_PIXEL,
    READ_INVALID_BITS,
    READ_INVALID_HEADER,
    READ_INVALID_TYPE,
    MEMORY_ALLOCATE_ERROR,
    READ_INVALID_SIGNATURE
};

/*  serializer   */
enum write_status {
    WRITE_OK = 0,
    WRITE_ERROR
    /* коды других ошибок  */
};

enum MODE {
    FROM_BMP_MODE = 0,
    TO_BMP_MODE = 1
};

enum ERRORS_INPUT{
    WRONG_COUNT_OF_ARGUMENTS = 1
};

#endif
